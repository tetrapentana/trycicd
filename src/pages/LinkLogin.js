import React, { Component } from 'react';
import Login from './Login';
import LinkForgot from './LinkForgot';
import './link-forgot.css';

class linkLogin extends Component {
    constructor(props){
      super(props);

      this.state = {
        loginOpen: true,
        forgotOpen: false
        
      };
      
    }

    toggleLogin = () => {
      this.setState ({
        loginOpen : !this.state.loginOpen,
        forgotOpen : (this.forgotOpen ? false:false),
      });
      console.log(this.state)
    }

    toggleForgot = () => {
      this.setState ({
        forgotOpen : !this.state.forgotOpen,
        loginOpen : (this.loginOpen ? false:false),
      });
      console.log(this.state)
    }
     

    
  render() {
    return (
      <div>
            <Login show={this.state.loginOpen} forgot={this.toggleForgot}>
          </Login>
          <LinkForgot show={this.state.forgotOpen} login={this.toggleLogin}></LinkForgot>
      </div>
    );
  } 
}

export default linkLogin;
