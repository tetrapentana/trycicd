import React, { Component } from 'react';
import './link-forgot.css';

// const divStyle = {
//     backgroundImage: 'rgb(6,156,113)',
//     backgroundImage: 'linear-gradient(90deg, rgba(6,156,113,1) 0%, rgba(9,74,121,1) 100%)',
//   };
  

class EmailLink extends Component {
    constructor() {
        super();
    
        this.state = {
          email: '',

        };
       
    }

    handleEmail(text){
        this.setState({ email:text.target.value })
        
     }
    
    EmailLink = (e) => {
        e.preventDefault();
        console.log(this.state); 
        let obj = {}
        obj.email = this.state.email;
    }
        
    
    
    render(){ 
            if(!this.props.show) {
                return null;
            }
            return (
                <div className="wrap">
                    <div className="forgotten-container">
                        <h1>Forgotten</h1>

                        <form onSubmit={(e) => {this.EmailLink(e);}} >
                            <div className="email-link-box">
                                <input type="email" 
                                name="email"
                                className="email-link" 
                                placeholder="E-mail" 
                                onChange ={(text) => {this.handleEmail(text)}} />
                            

                                <button type='submit' className="send-btn">Get new password</button>
                                <a href="#" className="forgot-link" onClick={this.props.login}>Back To Login </a>
                            </div>
                        </form>
                    </div> 
                </div>
            
            );
            
        
    }
}



    export default EmailLink;