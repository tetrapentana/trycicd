import React, { Component } from 'react';
import '../scss/components/login.css';

class App extends Component {
    state = {
        email: '',
        password: ''
    }

    handleEmail(text){
        this.setState({email:text.target.value})
    }
    handlePassword(text){
        this.setState({password:text.target.value})
    }

    login = (e) => {
        e.preventDefault();
        console.log(this.state);
        let obj = {}
        obj.email = this.state.email;
        obj.password = this.state.password



    fetch('https://staging-monggovest.herokuapp.com/api/v1/user_token/', {
        method: 'POST',
        headers: {
            'Content-Type':'application/json'
        },
        body: JSON.stringify({
            "auth": {
                "email": obj.email,
                "password": obj.password
            }

        }),

      
    })
}

  render() {
      if(!this.props.show) {
          return null;
      }
    return (
    <div className="login">
            {/* Close Button */}
            <div id="close-button"></div>
            
            <form onSubmit={(e) => {this.login(e);}}>
                <div className="form">
                    <label htmlFor="email-box" className="email-label">Email Address</label>
                    <input type="text" 
                    id="email-box"
                    onChange ={(text) => {this.handleEmail(text)}} required />
                </div> 
                
                <div>
                    <label htmlFor="pass-box" className="pass-label">Password</label>
                    <input type="password" 
                    name="" id="pass-box"  
                    onChange ={(text) => {this.handlePassword(text)}} required />

                    <div className="text">
                        <button type="submit" className="btn-login" >Login</button>
                    </div>
    
                    <p><i>Or Login With</i></p>
                    <div className="login-with">
                        <a href="https://facebook.com" className="facebook">
                        <i className='bx bxl-facebook-square bx-lg' ></i> </a>
                        <a href="https://plus.google.com" className="google-plus">
                        <i className='bx bxl-google-plus-circle bx-lg' ></i></a>
                        <a href="https://twitter.com" className="twitter">
                        <i className='bx bxl-twitter-square bx-lg'></i></a>
                    </div>

                    <div>
                        <a href="#" className="forgot" onClick={this.props.forgot}>Forgot Password? </a>
                    </div>   
                </div>

                
            </form>
    </div>  
    );
  }
}

export default App;
